/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sessionBeans;

import entities.Entrenamiento;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author JuanM
 */
@Stateless
public class EntrenamientoFacade extends AbstractFacade<Entrenamiento> {
    @PersistenceContext(unitName = "gestionClubFutbol-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EntrenamientoFacade() {
        super(Entrenamiento.class);
    }
    
    public List<Entrenamiento> getEntrenamientosPorEquipo(Integer equipoId) {
        EntityManager em = getEntityManager();
        List<Entrenamiento> res;
        try {
            Query q = em.createNamedQuery("Entrenamiento.findByEquipo");
            q.setParameter("equipoId", equipoId);
            res = q.getResultList();
        } catch (NoResultException e) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return res;
    }
    
}
