/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sessionBeans;

import entities.Entrenamiento;
import entities.Partido;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author JuanM
 */
@Stateless
public class PartidoFacade extends AbstractFacade<Partido> {
    @PersistenceContext(unitName = "gestionClubFutbol-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PartidoFacade() {
        super(Partido.class);
    }
        public List<Entrenamiento> getPartidosPorJornada(Integer jornadaId) {
        EntityManager em = getEntityManager();
        List<Entrenamiento> res;
        try {
            Query q = em.createNamedQuery("Partido.getPartidoByJornada");
            q.setParameter("jornadaId", jornadaId);
            res = q.getResultList();
        } catch (NoResultException e) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return res;
    }
}
