/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sessionBeans;

import entities.HistorialTemporada;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author JuanM
 */
@Stateless
public class HistorialTemporadaFacade extends AbstractFacade<HistorialTemporada> {
    @PersistenceContext(unitName = "gestionClubFutbol-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HistorialTemporadaFacade() {
        super(HistorialTemporada.class);
    }
    
}
