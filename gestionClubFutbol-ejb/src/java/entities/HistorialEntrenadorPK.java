/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author mbarrientos
 */
@Embeddable
public class HistorialEntrenadorPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "entrenador_id")
    private int entrenadorId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "equipo_id")
    private int equipoId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "temporada_idtemporada")
    @Temporal(TemporalType.DATE)
    private Date temporadaIdtemporada;

    public HistorialEntrenadorPK() {
    }

    public HistorialEntrenadorPK(int entrenadorId, int equipoId, Date temporadaIdtemporada) {
        this.entrenadorId = entrenadorId;
        this.equipoId = equipoId;
        this.temporadaIdtemporada = temporadaIdtemporada;
    }

    public int getEntrenadorId() {
        return entrenadorId;
    }

    public void setEntrenadorId(int entrenadorId) {
        this.entrenadorId = entrenadorId;
    }

    public int getEquipoId() {
        return equipoId;
    }

    public void setEquipoId(int equipoId) {
        this.equipoId = equipoId;
    }

    public Date getTemporadaIdtemporada() {
        return temporadaIdtemporada;
    }

    public void setTemporadaIdtemporada(Date temporadaIdtemporada) {
        this.temporadaIdtemporada = temporadaIdtemporada;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) entrenadorId;
        hash += (int) equipoId;
        hash += (temporadaIdtemporada != null ? temporadaIdtemporada.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistorialEntrenadorPK)) {
            return false;
        }
        HistorialEntrenadorPK other = (HistorialEntrenadorPK) object;
        if (this.entrenadorId != other.entrenadorId) {
            return false;
        }
        if (this.equipoId != other.equipoId) {
            return false;
        }
        if ((this.temporadaIdtemporada == null && other.temporadaIdtemporada != null) || (this.temporadaIdtemporada != null && !this.temporadaIdtemporada.equals(other.temporadaIdtemporada))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.HistorialEntrenadorPK[ entrenadorId=" + entrenadorId + ", equipoId=" + equipoId + ", temporadaIdtemporada=" + temporadaIdtemporada + " ]";
    }
    
}
