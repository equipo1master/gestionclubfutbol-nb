/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author mbarrientos
 */
@Embeddable
public class HistorialDelegadoPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "delegado_iddelegado")
    private int delegadoIddelegado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "equipo_id")
    private int equipoId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "temporada_idtemporada")
    @Temporal(TemporalType.DATE)
    private Date temporadaIdtemporada;

    public HistorialDelegadoPK() {
    }

    public HistorialDelegadoPK(int delegadoIddelegado, int equipoId, Date temporadaIdtemporada) {
        this.delegadoIddelegado = delegadoIddelegado;
        this.equipoId = equipoId;
        this.temporadaIdtemporada = temporadaIdtemporada;
    }

    public int getDelegadoIddelegado() {
        return delegadoIddelegado;
    }

    public void setDelegadoIddelegado(int delegadoIddelegado) {
        this.delegadoIddelegado = delegadoIddelegado;
    }

    public int getEquipoId() {
        return equipoId;
    }

    public void setEquipoId(int equipoId) {
        this.equipoId = equipoId;
    }

    public Date getTemporadaIdtemporada() {
        return temporadaIdtemporada;
    }

    public void setTemporadaIdtemporada(Date temporadaIdtemporada) {
        this.temporadaIdtemporada = temporadaIdtemporada;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) delegadoIddelegado;
        hash += (int) equipoId;
        hash += (temporadaIdtemporada != null ? temporadaIdtemporada.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistorialDelegadoPK)) {
            return false;
        }
        HistorialDelegadoPK other = (HistorialDelegadoPK) object;
        if (this.delegadoIddelegado != other.delegadoIddelegado) {
            return false;
        }
        if (this.equipoId != other.equipoId) {
            return false;
        }
        if ((this.temporadaIdtemporada == null && other.temporadaIdtemporada != null) || (this.temporadaIdtemporada != null && !this.temporadaIdtemporada.equals(other.temporadaIdtemporada))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.HistorialDelegadoPK[ delegadoIddelegado=" + delegadoIddelegado + ", equipoId=" + equipoId + ", temporadaIdtemporada=" + temporadaIdtemporada + " ]";
    }
    
}
