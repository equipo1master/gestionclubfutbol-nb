/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mbarrientos
 */
@Entity
@Table(name = "historial_liga")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HistorialLiga.findAll", query = "SELECT h FROM HistorialLiga h"),
    @NamedQuery(name = "HistorialLiga.findByEquipoId", query = "SELECT h FROM HistorialLiga h WHERE h.historialLigaPK.equipoId = :equipoId"),
    @NamedQuery(name = "HistorialLiga.findByLigaId", query = "SELECT h FROM HistorialLiga h WHERE h.historialLigaPK.ligaId = :ligaId"),
    @NamedQuery(name = "HistorialLiga.findByTemporadaIdtemporada", query = "SELECT h FROM HistorialLiga h WHERE h.historialLigaPK.temporadaIdtemporada = :temporadaIdtemporada")})
public class HistorialLiga implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected HistorialLigaPK historialLigaPK;
    @JoinColumn(name = "temporada_idtemporada", referencedColumnName = "idtemporada", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Temporada temporada;
    @JoinColumn(name = "liga_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Liga liga;
    @JoinColumn(name = "equipo_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Equipo equipo;

    public HistorialLiga() {
    }

    public HistorialLiga(HistorialLigaPK historialLigaPK) {
        this.historialLigaPK = historialLigaPK;
    }

    public HistorialLiga(int equipoId, int ligaId, Date temporadaIdtemporada) {
        this.historialLigaPK = new HistorialLigaPK(equipoId, ligaId, temporadaIdtemporada);
    }

    public HistorialLigaPK getHistorialLigaPK() {
        return historialLigaPK;
    }

    public void setHistorialLigaPK(HistorialLigaPK historialLigaPK) {
        this.historialLigaPK = historialLigaPK;
    }

    public Temporada getTemporada() {
        return temporada;
    }

    public void setTemporada(Temporada temporada) {
        this.temporada = temporada;
    }

    public Liga getLiga() {
        return liga;
    }

    public void setLiga(Liga liga) {
        this.liga = liga;
    }

    public Equipo getEquipo() {
        return equipo;
    }

    public void setEquipo(Equipo equipo) {
        this.equipo = equipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (historialLigaPK != null ? historialLigaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistorialLiga)) {
            return false;
        }
        HistorialLiga other = (HistorialLiga) object;
        if ((this.historialLigaPK == null && other.historialLigaPK != null) || (this.historialLigaPK != null && !this.historialLigaPK.equals(other.historialLigaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.HistorialLiga[ historialLigaPK=" + historialLigaPK + " ]";
    }
    
}
