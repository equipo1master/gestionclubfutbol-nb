/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author mbarrientos
 */
@Embeddable
public class HistorialJugadorPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "jugador")
    private int jugador;
    @Basic(optional = false)
    @NotNull
    @Column(name = "temporada")
    @Temporal(TemporalType.DATE)
    private Date temporada;
    @Basic(optional = false)
    @NotNull
    @Column(name = "equipo")
    private int equipo;

    public HistorialJugadorPK() {
    }

    public HistorialJugadorPK(int jugador, Date temporada, int equipo) {
        this.jugador = jugador;
        this.temporada = temporada;
        this.equipo = equipo;
    }

    public int getJugador() {
        return jugador;
    }

    public void setJugador(int jugador) {
        this.jugador = jugador;
    }

    public Date getTemporada() {
        return temporada;
    }

    public void setTemporada(Date temporada) {
        this.temporada = temporada;
    }

    public int getEquipo() {
        return equipo;
    }

    public void setEquipo(int equipo) {
        this.equipo = equipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) jugador;
        hash += (temporada != null ? temporada.hashCode() : 0);
        hash += (int) equipo;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistorialJugadorPK)) {
            return false;
        }
        HistorialJugadorPK other = (HistorialJugadorPK) object;
        if (this.jugador != other.jugador) {
            return false;
        }
        if ((this.temporada == null && other.temporada != null) || (this.temporada != null && !this.temporada.equals(other.temporada))) {
            return false;
        }
        if (this.equipo != other.equipo) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.HistorialJugadorPK[ jugador=" + jugador + ", temporada=" + temporada + ", equipo=" + equipo + " ]";
    }
    
}
