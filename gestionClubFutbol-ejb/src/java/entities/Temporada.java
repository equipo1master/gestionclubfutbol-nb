/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mbarrientos
 */
@Entity
@Table(name = "temporada")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Temporada.findAll", query = "SELECT t FROM Temporada t"),
    @NamedQuery(name = "Temporada.findByIdtemporada", query = "SELECT t FROM Temporada t WHERE t.idtemporada = :idtemporada")})
public class Temporada implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idtemporada")
    @Temporal(TemporalType.DATE)
    private Date idtemporada;
    @ManyToMany(mappedBy = "temporadaCollection")
    private Collection<Equipo> equipoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "temporadaIdtemporada")
    private Collection<Partido> partidoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "temporada")
    private Collection<HistorialTemporada> historialTemporadaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "temporada")
    private Collection<HistorialLiga> historialLigaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "temporada1")
    private Collection<HistorialJugador> historialJugadorCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "temporada")
    private Collection<Clasificacion> clasificacionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "temporada")
    private Collection<HistorialEntrenador> historialEntrenadorCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "temporada")
    private Collection<HistorialDelegado> historialDelegadoCollection;

    public Temporada() {
    }

    public Temporada(Date idtemporada) {
        this.idtemporada = idtemporada;
    }

    public Date getIdtemporada() {
        return idtemporada;
    }

    public void setIdtemporada(Date idtemporada) {
        this.idtemporada = idtemporada;
    }

    @XmlTransient
    public Collection<Equipo> getEquipoCollection() {
        return equipoCollection;
    }

    public void setEquipoCollection(Collection<Equipo> equipoCollection) {
        this.equipoCollection = equipoCollection;
    }

    @XmlTransient
    public Collection<Partido> getPartidoCollection() {
        return partidoCollection;
    }

    public void setPartidoCollection(Collection<Partido> partidoCollection) {
        this.partidoCollection = partidoCollection;
    }

    @XmlTransient
    public Collection<HistorialTemporada> getHistorialTemporadaCollection() {
        return historialTemporadaCollection;
    }

    public void setHistorialTemporadaCollection(Collection<HistorialTemporada> historialTemporadaCollection) {
        this.historialTemporadaCollection = historialTemporadaCollection;
    }

    @XmlTransient
    public Collection<HistorialLiga> getHistorialLigaCollection() {
        return historialLigaCollection;
    }

    public void setHistorialLigaCollection(Collection<HistorialLiga> historialLigaCollection) {
        this.historialLigaCollection = historialLigaCollection;
    }

    @XmlTransient
    public Collection<HistorialJugador> getHistorialJugadorCollection() {
        return historialJugadorCollection;
    }

    public void setHistorialJugadorCollection(Collection<HistorialJugador> historialJugadorCollection) {
        this.historialJugadorCollection = historialJugadorCollection;
    }

    @XmlTransient
    public Collection<Clasificacion> getClasificacionCollection() {
        return clasificacionCollection;
    }

    public void setClasificacionCollection(Collection<Clasificacion> clasificacionCollection) {
        this.clasificacionCollection = clasificacionCollection;
    }

    @XmlTransient
    public Collection<HistorialEntrenador> getHistorialEntrenadorCollection() {
        return historialEntrenadorCollection;
    }

    public void setHistorialEntrenadorCollection(Collection<HistorialEntrenador> historialEntrenadorCollection) {
        this.historialEntrenadorCollection = historialEntrenadorCollection;
    }

    @XmlTransient
    public Collection<HistorialDelegado> getHistorialDelegadoCollection() {
        return historialDelegadoCollection;
    }

    public void setHistorialDelegadoCollection(Collection<HistorialDelegado> historialDelegadoCollection) {
        this.historialDelegadoCollection = historialDelegadoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtemporada != null ? idtemporada.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Temporada)) {
            return false;
        }
        Temporada other = (Temporada) object;
        if ((this.idtemporada == null && other.idtemporada != null) || (this.idtemporada != null && !this.idtemporada.equals(other.idtemporada))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Temporada[ idtemporada=" + idtemporada + " ]";
    }
    
}
