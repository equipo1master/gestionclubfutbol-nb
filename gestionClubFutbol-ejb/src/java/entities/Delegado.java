/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mbarrientos
 */
@Entity
@Table(name = "delegado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Delegado.findAll", query = "SELECT d FROM Delegado d"),
    @NamedQuery(name = "Delegado.findByIddelegado", query = "SELECT d FROM Delegado d WHERE d.iddelegado = :iddelegado"),
    @NamedQuery(name = "Delegado.findByNombre", query = "SELECT d FROM Delegado d WHERE d.nombre = :nombre"),
    @NamedQuery(name = "Delegado.findByApellido", query = "SELECT d FROM Delegado d WHERE d.apellido = :apellido"),
    @NamedQuery(name = "Delegado.findByTelefono", query = "SELECT d FROM Delegado d WHERE d.telefono = :telefono"),
    @NamedQuery(name = "Delegado.findByEmail", query = "SELECT d FROM Delegado d WHERE d.email = :email")})
public class Delegado implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "iddelegado")
    private Integer iddelegado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "apellido")
    private String apellido;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "telefono")
    private String telefono;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 45)
    @Column(name = "email")
    private String email;
    @JoinColumn(name = "equipo_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Equipo equipoId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "delegado")
    private Collection<HistorialDelegado> historialDelegadoCollection;

    public Delegado() {
    }

    public Delegado(Integer iddelegado) {
        this.iddelegado = iddelegado;
    }

    public Delegado(Integer iddelegado, String nombre, String apellido, String telefono) {
        this.iddelegado = iddelegado;
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
    }

    public Integer getIddelegado() {
        return iddelegado;
    }

    public void setIddelegado(Integer iddelegado) {
        this.iddelegado = iddelegado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Equipo getEquipoId() {
        return equipoId;
    }

    public void setEquipoId(Equipo equipoId) {
        this.equipoId = equipoId;
    }

    @XmlTransient
    public Collection<HistorialDelegado> getHistorialDelegadoCollection() {
        return historialDelegadoCollection;
    }

    public void setHistorialDelegadoCollection(Collection<HistorialDelegado> historialDelegadoCollection) {
        this.historialDelegadoCollection = historialDelegadoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddelegado != null ? iddelegado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Delegado)) {
            return false;
        }
        Delegado other = (Delegado) object;
        if ((this.iddelegado == null && other.iddelegado != null) || (this.iddelegado != null && !this.iddelegado.equals(other.iddelegado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Delegado[ iddelegado=" + iddelegado + " ]";
    }
    
}
