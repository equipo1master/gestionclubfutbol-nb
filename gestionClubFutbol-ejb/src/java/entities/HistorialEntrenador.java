/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mbarrientos
 */
@Entity
@Table(name = "historial_entrenador")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HistorialEntrenador.findAll", query = "SELECT h FROM HistorialEntrenador h"),
    @NamedQuery(name = "HistorialEntrenador.findByEntrenadorId", query = "SELECT h FROM HistorialEntrenador h WHERE h.historialEntrenadorPK.entrenadorId = :entrenadorId"),
    @NamedQuery(name = "HistorialEntrenador.findByEquipoId", query = "SELECT h FROM HistorialEntrenador h WHERE h.historialEntrenadorPK.equipoId = :equipoId"),
    @NamedQuery(name = "HistorialEntrenador.findByTemporadaIdtemporada", query = "SELECT h FROM HistorialEntrenador h WHERE h.historialEntrenadorPK.temporadaIdtemporada = :temporadaIdtemporada")})
public class HistorialEntrenador implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected HistorialEntrenadorPK historialEntrenadorPK;
    @JoinColumn(name = "temporada_idtemporada", referencedColumnName = "idtemporada", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Temporada temporada;
    @JoinColumn(name = "equipo_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Equipo equipo;
    @JoinColumn(name = "entrenador_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Entrenador entrenador;

    public HistorialEntrenador() {
    }

    public HistorialEntrenador(HistorialEntrenadorPK historialEntrenadorPK) {
        this.historialEntrenadorPK = historialEntrenadorPK;
    }

    public HistorialEntrenador(int entrenadorId, int equipoId, Date temporadaIdtemporada) {
        this.historialEntrenadorPK = new HistorialEntrenadorPK(entrenadorId, equipoId, temporadaIdtemporada);
    }

    public HistorialEntrenadorPK getHistorialEntrenadorPK() {
        return historialEntrenadorPK;
    }

    public void setHistorialEntrenadorPK(HistorialEntrenadorPK historialEntrenadorPK) {
        this.historialEntrenadorPK = historialEntrenadorPK;
    }

    public Temporada getTemporada() {
        return temporada;
    }

    public void setTemporada(Temporada temporada) {
        this.temporada = temporada;
    }

    public Equipo getEquipo() {
        return equipo;
    }

    public void setEquipo(Equipo equipo) {
        this.equipo = equipo;
    }

    public Entrenador getEntrenador() {
        return entrenador;
    }

    public void setEntrenador(Entrenador entrenador) {
        this.entrenador = entrenador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (historialEntrenadorPK != null ? historialEntrenadorPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistorialEntrenador)) {
            return false;
        }
        HistorialEntrenador other = (HistorialEntrenador) object;
        if ((this.historialEntrenadorPK == null && other.historialEntrenadorPK != null) || (this.historialEntrenadorPK != null && !this.historialEntrenadorPK.equals(other.historialEntrenadorPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.HistorialEntrenador[ historialEntrenadorPK=" + historialEntrenadorPK + " ]";
    }
    
}
