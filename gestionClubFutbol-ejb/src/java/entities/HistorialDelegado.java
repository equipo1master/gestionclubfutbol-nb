/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mbarrientos
 */
@Entity
@Table(name = "historial_delegado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HistorialDelegado.findAll", query = "SELECT h FROM HistorialDelegado h"),
    @NamedQuery(name = "HistorialDelegado.findByDelegadoIddelegado", query = "SELECT h FROM HistorialDelegado h WHERE h.historialDelegadoPK.delegadoIddelegado = :delegadoIddelegado"),
    @NamedQuery(name = "HistorialDelegado.findByEquipoId", query = "SELECT h FROM HistorialDelegado h WHERE h.historialDelegadoPK.equipoId = :equipoId"),
    @NamedQuery(name = "HistorialDelegado.findByTemporadaIdtemporada", query = "SELECT h FROM HistorialDelegado h WHERE h.historialDelegadoPK.temporadaIdtemporada = :temporadaIdtemporada")})
public class HistorialDelegado implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected HistorialDelegadoPK historialDelegadoPK;
    @JoinColumn(name = "temporada_idtemporada", referencedColumnName = "idtemporada", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Temporada temporada;
    @JoinColumn(name = "equipo_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Equipo equipo;
    @JoinColumn(name = "delegado_iddelegado", referencedColumnName = "iddelegado", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Delegado delegado;

    public HistorialDelegado() {
    }

    public HistorialDelegado(HistorialDelegadoPK historialDelegadoPK) {
        this.historialDelegadoPK = historialDelegadoPK;
    }

    public HistorialDelegado(int delegadoIddelegado, int equipoId, Date temporadaIdtemporada) {
        this.historialDelegadoPK = new HistorialDelegadoPK(delegadoIddelegado, equipoId, temporadaIdtemporada);
    }

    public HistorialDelegadoPK getHistorialDelegadoPK() {
        return historialDelegadoPK;
    }

    public void setHistorialDelegadoPK(HistorialDelegadoPK historialDelegadoPK) {
        this.historialDelegadoPK = historialDelegadoPK;
    }

    public Temporada getTemporada() {
        return temporada;
    }

    public void setTemporada(Temporada temporada) {
        this.temporada = temporada;
    }

    public Equipo getEquipo() {
        return equipo;
    }

    public void setEquipo(Equipo equipo) {
        this.equipo = equipo;
    }

    public Delegado getDelegado() {
        return delegado;
    }

    public void setDelegado(Delegado delegado) {
        this.delegado = delegado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (historialDelegadoPK != null ? historialDelegadoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistorialDelegado)) {
            return false;
        }
        HistorialDelegado other = (HistorialDelegado) object;
        if ((this.historialDelegadoPK == null && other.historialDelegadoPK != null) || (this.historialDelegadoPK != null && !this.historialDelegadoPK.equals(other.historialDelegadoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.HistorialDelegado[ historialDelegadoPK=" + historialDelegadoPK + " ]";
    }
    
}
