/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JuanM
 */
@Entity
@Table(name = "partido")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Partido.findAll", query = "SELECT p FROM Partido p"),
    @NamedQuery(name = "Partido.findById", query = "SELECT p FROM Partido p WHERE p.id = :id"),
    @NamedQuery(name = "Partido.findByResultado", query = "SELECT p FROM Partido p WHERE p.resultado = :resultado"),
    @NamedQuery(name = "Partido.findByEquipoRival", query = "SELECT p FROM Partido p WHERE p.equipoRival = :equipoRival"),
    @NamedQuery(name = "Partido.findByJuegoEnCasa", query = "SELECT p FROM Partido p WHERE p.juegoEnCasa = :juegoEnCasa"),
    @NamedQuery(name = "Partido.findByJornada", query = "SELECT p FROM Partido p WHERE p.jornada = :jornada"),
    @NamedQuery(name = "Partido.findByFecha", query = "SELECT p FROM Partido p WHERE p.fecha = :fecha"),
    @NamedQuery(name = "Partido.getPartidoByJornada", query = "SELECT p FROM Partido p WHERE p.jornada = :jornadaId")})
    

public class Partido implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 45)
    @Column(name = "resultado")
    private String resultado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "equipo_rival")
    private String equipoRival;
    @Column(name = "juego_en_casa")
    private Boolean juegoEnCasa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "jornada")
    private int jornada;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "partidoId")
    private Collection<Convocatoria> convocatoriaCollection;
    @JoinColumn(name = "temporada_idtemporada", referencedColumnName = "idtemporada")
    @ManyToOne(optional = false)
    private Temporada temporadaIdtemporada;
    @JoinColumn(name = "equipo", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Equipo equipo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "partidoId")
    private Collection<Gol> golCollection;

    public Partido() {
    }

    public Partido(Integer id) {
        this.id = id;
    }

    public Partido(Integer id, String equipoRival, int jornada, Date fecha) {
        this.id = id;
        this.equipoRival = equipoRival;
        this.jornada = jornada;
        this.fecha = fecha;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getEquipoRival() {
        return equipoRival;
    }

    public void setEquipoRival(String equipoRival) {
        this.equipoRival = equipoRival;
    }

    public Boolean getJuegoEnCasa() {
        return juegoEnCasa;
    }

    public void setJuegoEnCasa(Boolean juegoEnCasa) {
        this.juegoEnCasa = juegoEnCasa;
    }

    public int getJornada() {
        return jornada;
    }

    public void setJornada(int jornada) {
        this.jornada = jornada;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @XmlTransient
    public Collection<Convocatoria> getConvocatoriaCollection() {
        return convocatoriaCollection;
    }

    public void setConvocatoriaCollection(Collection<Convocatoria> convocatoriaCollection) {
        this.convocatoriaCollection = convocatoriaCollection;
    }

    public Temporada getTemporadaIdtemporada() {
        return temporadaIdtemporada;
    }

    public void setTemporadaIdtemporada(Temporada temporadaIdtemporada) {
        this.temporadaIdtemporada = temporadaIdtemporada;
    }

    public Equipo getEquipo() {
        return equipo;
    }

    public void setEquipo(Equipo equipo) {
        this.equipo = equipo;
    }

    @XmlTransient
    public Collection<Gol> getGolCollection() {
        return golCollection;
    }

    public void setGolCollection(Collection<Gol> golCollection) {
        this.golCollection = golCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Partido)) {
            return false;
        }
        Partido other = (Partido) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Partido[ id=" + id + " ]";
    }
    
}
