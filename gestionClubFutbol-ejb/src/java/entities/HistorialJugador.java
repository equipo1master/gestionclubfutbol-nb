/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mbarrientos
 */
@Entity
@Table(name = "historial_jugador")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HistorialJugador.findAll", query = "SELECT h FROM HistorialJugador h"),
    @NamedQuery(name = "HistorialJugador.findByFechaAlta", query = "SELECT h FROM HistorialJugador h WHERE h.fechaAlta = :fechaAlta"),
    @NamedQuery(name = "HistorialJugador.findByJugador", query = "SELECT h FROM HistorialJugador h WHERE h.historialJugadorPK.jugador = :jugador"),
    @NamedQuery(name = "HistorialJugador.findByTemporada", query = "SELECT h FROM HistorialJugador h WHERE h.historialJugadorPK.temporada = :temporada"),
    @NamedQuery(name = "HistorialJugador.findByEquipo", query = "SELECT h FROM HistorialJugador h WHERE h.historialJugadorPK.equipo = :equipo")})
public class HistorialJugador implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected HistorialJugadorPK historialJugadorPK;
    @Column(name = "fecha_alta")
    @Temporal(TemporalType.DATE)
    private Date fechaAlta;
    @JoinColumn(name = "temporada", referencedColumnName = "idtemporada", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Temporada temporada1;
    @JoinColumn(name = "jugador", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Jugador jugador1;
    @JoinColumn(name = "equipo", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Equipo equipo1;

    public HistorialJugador() {
    }

    public HistorialJugador(HistorialJugadorPK historialJugadorPK) {
        this.historialJugadorPK = historialJugadorPK;
    }

    public HistorialJugador(int jugador, Date temporada, int equipo) {
        this.historialJugadorPK = new HistorialJugadorPK(jugador, temporada, equipo);
    }

    public HistorialJugadorPK getHistorialJugadorPK() {
        return historialJugadorPK;
    }

    public void setHistorialJugadorPK(HistorialJugadorPK historialJugadorPK) {
        this.historialJugadorPK = historialJugadorPK;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Temporada getTemporada1() {
        return temporada1;
    }

    public void setTemporada1(Temporada temporada1) {
        this.temporada1 = temporada1;
    }

    public Jugador getJugador1() {
        return jugador1;
    }

    public void setJugador1(Jugador jugador1) {
        this.jugador1 = jugador1;
    }

    public Equipo getEquipo1() {
        return equipo1;
    }

    public void setEquipo1(Equipo equipo1) {
        this.equipo1 = equipo1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (historialJugadorPK != null ? historialJugadorPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistorialJugador)) {
            return false;
        }
        HistorialJugador other = (HistorialJugador) object;
        if ((this.historialJugadorPK == null && other.historialJugadorPK != null) || (this.historialJugadorPK != null && !this.historialJugadorPK.equals(other.historialJugadorPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.HistorialJugador[ historialJugadorPK=" + historialJugadorPK + " ]";
    }
    
}
