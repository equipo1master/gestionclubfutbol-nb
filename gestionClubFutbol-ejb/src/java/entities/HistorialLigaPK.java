/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author mbarrientos
 */
@Embeddable
public class HistorialLigaPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "equipo_id")
    private int equipoId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "liga_id")
    private int ligaId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "temporada_idtemporada")
    @Temporal(TemporalType.DATE)
    private Date temporadaIdtemporada;

    public HistorialLigaPK() {
    }

    public HistorialLigaPK(int equipoId, int ligaId, Date temporadaIdtemporada) {
        this.equipoId = equipoId;
        this.ligaId = ligaId;
        this.temporadaIdtemporada = temporadaIdtemporada;
    }

    public int getEquipoId() {
        return equipoId;
    }

    public void setEquipoId(int equipoId) {
        this.equipoId = equipoId;
    }

    public int getLigaId() {
        return ligaId;
    }

    public void setLigaId(int ligaId) {
        this.ligaId = ligaId;
    }

    public Date getTemporadaIdtemporada() {
        return temporadaIdtemporada;
    }

    public void setTemporadaIdtemporada(Date temporadaIdtemporada) {
        this.temporadaIdtemporada = temporadaIdtemporada;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) equipoId;
        hash += (int) ligaId;
        hash += (temporadaIdtemporada != null ? temporadaIdtemporada.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistorialLigaPK)) {
            return false;
        }
        HistorialLigaPK other = (HistorialLigaPK) object;
        if (this.equipoId != other.equipoId) {
            return false;
        }
        if (this.ligaId != other.ligaId) {
            return false;
        }
        if ((this.temporadaIdtemporada == null && other.temporadaIdtemporada != null) || (this.temporadaIdtemporada != null && !this.temporadaIdtemporada.equals(other.temporadaIdtemporada))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.HistorialLigaPK[ equipoId=" + equipoId + ", ligaId=" + ligaId + ", temporadaIdtemporada=" + temporadaIdtemporada + " ]";
    }
    
}
