/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mbarrientos
 */
@Entity
@Table(name = "equipo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Equipo.findAll", query = "SELECT e FROM Equipo e"),
    @NamedQuery(name = "Equipo.findById", query = "SELECT e FROM Equipo e WHERE e.id = :id"),
    @NamedQuery(name = "Equipo.findByCategoria", query = "SELECT e FROM Equipo e WHERE e.categoria = :categoria"),
    @NamedQuery(name = "Equipo.findByNombre", query = "SELECT e FROM Equipo e WHERE e.nombre = :nombre")})
public class Equipo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "categoria")
    private String categoria;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nombre")
    private String nombre;
    @JoinTable(name = "temporada_equipo", joinColumns = {
        @JoinColumn(name = "equipo_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "temporada_idtemporada", referencedColumnName = "idtemporada")})
    @ManyToMany
    private Collection<Temporada> temporadaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "equipo")
    private Collection<Jugador> jugadorCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "equipoId")
    private Collection<Convocatoria> convocatoriaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "equipoId")
    private Collection<Entrenamiento> entrenamientoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "equipo")
    private Collection<Partido> partidoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "equipo")
    private Collection<HistorialTemporada> historialTemporadaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "equipo")
    private Collection<HistorialLiga> historialLigaCollection;
    @JoinColumn(name = "entrenador", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Entrenador entrenador;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "equipo1")
    private Collection<HistorialJugador> historialJugadorCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "grupoEquipo")
    private Collection<Clasificacion> clasificacionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "equipoId")
    private Collection<Delegado> delegadoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "equipo")
    private Collection<HistorialEntrenador> historialEntrenadorCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "equipo")
    private Collection<HistorialDelegado> historialDelegadoCollection;

    public Equipo() {
    }

    public Equipo(Integer id) {
        this.id = id;
    }

    public Equipo(Integer id, String categoria, String nombre) {
        this.id = id;
        this.categoria = categoria;
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public Collection<Temporada> getTemporadaCollection() {
        return temporadaCollection;
    }

    public void setTemporadaCollection(Collection<Temporada> temporadaCollection) {
        this.temporadaCollection = temporadaCollection;
    }

    @XmlTransient
    public Collection<Jugador> getJugadorCollection() {
        return jugadorCollection;
    }

    public void setJugadorCollection(Collection<Jugador> jugadorCollection) {
        this.jugadorCollection = jugadorCollection;
    }

    @XmlTransient
    public Collection<Convocatoria> getConvocatoriaCollection() {
        return convocatoriaCollection;
    }

    public void setConvocatoriaCollection(Collection<Convocatoria> convocatoriaCollection) {
        this.convocatoriaCollection = convocatoriaCollection;
    }

    @XmlTransient
    public Collection<Entrenamiento> getEntrenamientoCollection() {
        return entrenamientoCollection;
    }

    public void setEntrenamientoCollection(Collection<Entrenamiento> entrenamientoCollection) {
        this.entrenamientoCollection = entrenamientoCollection;
    }

    @XmlTransient
    public Collection<Partido> getPartidoCollection() {
        return partidoCollection;
    }

    public void setPartidoCollection(Collection<Partido> partidoCollection) {
        this.partidoCollection = partidoCollection;
    }

    @XmlTransient
    public Collection<HistorialTemporada> getHistorialTemporadaCollection() {
        return historialTemporadaCollection;
    }

    public void setHistorialTemporadaCollection(Collection<HistorialTemporada> historialTemporadaCollection) {
        this.historialTemporadaCollection = historialTemporadaCollection;
    }

    @XmlTransient
    public Collection<HistorialLiga> getHistorialLigaCollection() {
        return historialLigaCollection;
    }

    public void setHistorialLigaCollection(Collection<HistorialLiga> historialLigaCollection) {
        this.historialLigaCollection = historialLigaCollection;
    }

    public Entrenador getEntrenador() {
        return entrenador;
    }

    public void setEntrenador(Entrenador entrenador) {
        this.entrenador = entrenador;
    }

    @XmlTransient
    public Collection<HistorialJugador> getHistorialJugadorCollection() {
        return historialJugadorCollection;
    }

    public void setHistorialJugadorCollection(Collection<HistorialJugador> historialJugadorCollection) {
        this.historialJugadorCollection = historialJugadorCollection;
    }

    @XmlTransient
    public Collection<Clasificacion> getClasificacionCollection() {
        return clasificacionCollection;
    }

    public void setClasificacionCollection(Collection<Clasificacion> clasificacionCollection) {
        this.clasificacionCollection = clasificacionCollection;
    }

    @XmlTransient
    public Collection<Delegado> getDelegadoCollection() {
        return delegadoCollection;
    }

    public void setDelegadoCollection(Collection<Delegado> delegadoCollection) {
        this.delegadoCollection = delegadoCollection;
    }

    @XmlTransient
    public Collection<HistorialEntrenador> getHistorialEntrenadorCollection() {
        return historialEntrenadorCollection;
    }

    public void setHistorialEntrenadorCollection(Collection<HistorialEntrenador> historialEntrenadorCollection) {
        this.historialEntrenadorCollection = historialEntrenadorCollection;
    }

    @XmlTransient
    public Collection<HistorialDelegado> getHistorialDelegadoCollection() {
        return historialDelegadoCollection;
    }

    public void setHistorialDelegadoCollection(Collection<HistorialDelegado> historialDelegadoCollection) {
        this.historialDelegadoCollection = historialDelegadoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Equipo)) {
            return false;
        }
        Equipo other = (Equipo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Equipo[ id=" + id + " ]";
    }
    
}
