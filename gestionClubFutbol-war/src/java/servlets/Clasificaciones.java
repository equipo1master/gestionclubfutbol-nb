/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import entities.Clasificacion;
import entities.Equipo;
import entities.HistorialLiga;
import entities.Partido;
import entities.Temporada;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sessionBeans.ClasificacionFacade;
import sessionBeans.EquipoFacade;
import sessionBeans.HistorialLigaFacade;
import sessionBeans.HistorialTemporadaFacade;
import sessionBeans.LigaFacade;
import sessionBeans.PartidoFacade;
import sessionBeans.TemporadaFacade;

/**
 *
 * @author Juan Enrique
 */
@WebServlet(name = "clasificaciones", urlPatterns = {"/clasificaciones"})
public class Clasificaciones extends HttpServlet {
    @EJB
    private TemporadaFacade temporadaFacade;
    @EJB
    private PartidoFacade partidoFacade;
    @EJB
    private LigaFacade ligaFacade;
    @EJB
    private HistorialTemporadaFacade historialTemporadaFacade;
    @EJB
    private HistorialLigaFacade historialLigaFacade;
    @EJB
    private EquipoFacade equipoFacade;
    @EJB
    private ClasificacionFacade clasificacionFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ctrClasificaciones</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ctrClasificaciones at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Temporada tempActual = temporadaFacade.find(new Date());
        List<HistorialLiga> ligasequipos = historialLigaFacade.findAll();
        List<Partido> partidos = partidoFacade.findAll();
        List<Clasificacion> posiciones = clasificacionFacade.findAll();
        List<Equipo> equipos = equipoFacade.findAll();
        
        request.setAttribute("tempActual", tempActual);
        request.setAttribute("ligasequipos", ligasequipos);
        request.setAttribute("partidos", partidos);
        request.setAttribute("posiciones", posiciones);
        request.setAttribute("equipos", equipos);
        
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/clasificaciones.jsp");
        dispatcher.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
