/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;

import entities.Entrenamiento;
import entities.Equipo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sessionBeans.EntrenamientoFacade;
import sessionBeans.EquipoFacade;

/**
 *
 * @author JuanM
 */
@WebServlet(name = "entrenamientos", urlPatterns = {"/entrenamientos"})
public class Entrenamientos extends HttpServlet {
    @EJB
    private EquipoFacade equipoFacade;
    @EJB
    private EntrenamientoFacade entrenamientoFacade;

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //Obtiene una lista con todos los equipos
        List<Equipo> equipos = equipoFacade.findAll();
        
        //Añade la lista como atributo de request
	request.setAttribute("equipos", equipos);
				
	//get the request dispatcher
	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/entrenamientos.jsp");
		
	//forward to the jsp file to display the book list
	dispatcher.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
