/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import entities.Equipo;
import entities.Temporada;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sessionBeans.EntrenamientoFacade;
import sessionBeans.EquipoFacade;
import sessionBeans.PartidoFacade;
import sessionBeans.TemporadaFacade;

@WebServlet(name = "Historico", urlPatterns = {"/historico"})
public class Historico extends HttpServlet {

    @EJB
    private EquipoFacade equipoFacade;
    @EJB
    private EntrenamientoFacade entrenamientoFacade;
    @EJB
    private TemporadaFacade temporadaFacade;
    @EJB
    private PartidoFacade partidoFacade;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<String> error = new LinkedList<>();
        request.setAttribute("error", error);

        Integer equipoId = (Integer) request.getAttribute("equipo");
        String categoria = (String) request.getAttribute("categoria");
        
        if (equipoId == null) {
            error.add("El campo Equipo es obligatorio.");
        }
        if (categoria == null) {
            error.add("El campo categoría es obligatorio. Por favor marque la categoría de su equipo.");
        }
        
        request.setAttribute("equipos", equipoFacade.findAll());
        request.setAttribute("partidos", partidoFacade.findAll());

<<<<<<< Updated upstream
        Equipo equipo = equipoFacade.find(1);
        request.setAttribute("equipo", equipo);
        
        List<Temporada> temporadas = temporadaFacade.findAll();
        
        request.setAttribute("partidos", partidoFacade.findAll());

        //EntityManagerFactory factory = Persistence.createEntityManagerFactory();
=======
>>>>>>> Stashed changes
        //get the request dispatcher
        RequestDispatcher dispatcher = request.getRequestDispatcher("/historico.jsp");
        
        //forward to the jsp file to display the book list
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
}
