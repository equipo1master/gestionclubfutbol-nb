<%-- 
    Document   : jornadas
    Created on : Nov 6, 2014, 1:38:12 PM
    Author     : mbarrientos
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="css/propio.css">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="css/bootstrap.css">
		<!-- Optional theme -->
		<link rel="stylesheet" href="css/bootstrap-theme.css">
		<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">

		<script src="js/jquery-2.1.1.min.js"></script>

		<!-- Latest compiled and minified JavaScript -->
		<script src="js/bootstrap.js"></script>

		<script type="text/javascript" src="js/nav.js"></script>
	</head>

	<body>
<!-- ************************************************************************************************************** -->
		<span class="ir-arriba icon-arrow-up2">Ir arriba</span>
		<nav class="navbar navbar-inverse" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#"><i class="fa fa-shield"></i> Manager FC</a>
		    </div>
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
                        <li><a href="${pageContext.request.contextPath}/entrenamientos"><i class="fa fa-soccer-ball-o"></i> Entrenamientos</a></li>
                        <li class="active"><a href="${pageContext.request.contextPath}/jornadas"><i class="fa fa-calendar"></i> Jornadas</a></li>
                        <li><a href="${pageContext.request.contextPath}/clasificaciones"><i class="fa fa-line-chart"></i> Clasificaciones</a></li>
                        <li><a href="${pageContext.request.contextPath}/historico"><i class="fa fa-history"></i> Histórico</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="${pageContext.request.contextPath}"><img id="logo" src="images/logo.png"></img></a></li>
                    </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>


		<!-- ****************************************************************************************** -->
		<!-- ******************************************CONTENIDO*************************************** -->
		<!-- ****************************************************************************************** -->
		<div id="principal" class="container">
			
			<div class="page-header">
  				<h1><i class="fa fa-calendar"></i> Jornadas <small>Equipos</small></h1>
			</div>
			<!-- ****************************************************************************************** -->
			<!-- ******************************************EQUIPOS***************************************** -->
			<!-- ****************************************************************************************** -->
			
			<div class="btn-group btn-group-justified">
			  <div class="btn-group">
			    <button type="button" class="btn btn-default">Prebenjamín</button>
			  </div>
			  <div class="btn-group">
			    <button type="button" class="btn btn-default">Benjamín</button>
			  </div>
			  <div class="btn-group">
			    <button type="button" class="btn btn-default">Alevín</button>
			  </div>
			  <div class="btn-group">
			    <button type="button" class="btn btn-default">Infantil</button>
			  </div>
			  <div class="btn-group">
			    <button type="button" class="btn btn-default">Cadete</button>
			  </div>
			  <div class="btn-group">
			    <button type="button" class="btn btn-default">Senior</button>
			  </div>
			</div>
			<div style="float: left" class="input-group col-md-10">
				<span class="input-group-addon"><i class="fa fa-shield"></i></span>
				<input type="text" class="form-control" placeholder="Equipo">
			</div>
			<div style="float: right; padding: 0px;" class="col-md-2">
				<button type="submit" class="btn btn-relieve-cesped col-md-12">Enviar</button>
			</div>
			<ul class="pagination" style="padding-left: auto">
			  <li><a href="#">&laquo;</a></li>
			  <li><a href="#">1</a></li>
			  <li><a href="#">2</a></li>
			  <li><a href="#">3</a></li>
			  <li><a href="#">4</a></li>
			  <li><a href="#">5</a></li>
			  <li><a href="#">&raquo;</a></li>
			</ul>
			<!-- ****************************************************************************************** -->
			<!-- *****************************************PARTIDOS***************************************** -->
			<!-- ****************************************************************************************** -->
			<div class="page-header">
			  <h3><i class="fa fa-soccer-ball-o"></i> Partidos</h3>
			</div>
			<table class="table table-striped table-hover table-bordered table-verde">
				<thead>
					<tr>
						<th>Partido</th>
						<th>Día</th>
						<th>Horario</th>
					</tr>
				</thead>
				<tbody>
					<tr>
					  <td>Real Madrid - Barcelona</td>
					  <td>Lunes 8 de Octubre</td>
					  <td>4:30 / 6:30</td>
					</tr>
					<tr>
					  <td>Betis - Real Madrid</td>
					  <td>Miércoles 10 de Octubre</td>
					  <td>4:30 / 6:30</td>
					</tr>
				</tbody>
			</table>
			<!-- ****************************************************************************************** -->
			<!-- ***************************************CONVOCATORIA*************************************** -->
			<!-- ****************************************************************************************** -->
			
			<div class="page-header" style="margin-top: 20px;">
			  <h3><i class="fa fa-group"></i> Convocatoria</h3>
			</div>
			<table class="table table-bordered table-pizarra table-verde">
				<thead>
					<tr>
						<th><i></i></th>
						<th>Nombre</th>
						<th>Edad</th>
						<th>Posición</th>
					</tr>
				</thead>
				<tbody>
					<tr>
					  <td>1</td>
					  <td>Bodo Illgner</td>
					  <td>31</td>
					  <td>Portero</td>
					</tr>
					<tr>
					  <td>2</td>
					  <td>Christian Panucci</td>
					  <td>25</td>
					  <td>Defensa</td>
					</tr>
					<tr>
					  <td>3</td>
					  <td>Roberto Carlos Da Silva Rocha</td>
					  <td>25</td>
					  <td>Defensa</td>
					</tr>
					<tr>
					  <td>4</td>
					  <td>Fernando Ruiz Hierro</td>
					  <td>30</td>
					  <td>Defensa</td>
					</tr>
					<tr>
					  <td>5</td>
					  <td>Manuel Sanchis Hontiyuelo</td>
					  <td>33</td>
					  <td>Defensa</td>
					</tr>
					<tr>
					  <td>10</td>
					  <td>Clarence Seedorf</td>
					  <td>22</td>
					  <td>Medio Campista</td>
					</tr>
					<tr>
					  <td>20</td>
					  <td>Savio Bortolini Pimentel</td>
					  <td>22</td>
					  <td>Medio Campista</td>
					</tr>
					<tr>
					  <td>6</td>
					  <td>Fernando Carlos Redondo Neri</td>
					  <td>22</td>
					  <td>Medio Campista</td>
					</tr>
					<tr>
					  <td>7</td>
					  <td>Raúl González Blanco</td>
					  <td>22</td>
					  <td>Delantero</td>
					</tr>
					<tr>
					  <td>8</td>
					  <td>Predrag Mijatovic</td>
					  <td>22</td>
					  <td>Delantero</td>
					</tr>
					<tr>
					  <td>15</td>
					  <td>Fernando Morientes Sánchez</td>
					  <td>22</td>
					  <td>Delantero</td>
					</tr>
				</tbody>
			</table>
			<!-- ****************************************************************************************** -->
			<!-- ***************************************CLASIFICACIÓN************************************** -->
			<!-- ****************************************************************************************** -->
			<div class="page-header">
			  <h3><i class="fa fa-line-chart"></i> Clasificación</h3>
			</div>
			<table class="table table-bordered table-verde">
				<tr>
					<th>Equipo</th>
					<th>Posición</th>
					<th>Goles</th>
				</tr>
				<tr>
				  <td class="success">Real Madrid</td>
				  <td class="success">1</td>
				  <td class="success">4</td>
				</td>
				<tr>
				  <td class="info">Real Betis Balonpié</td>
				  <td class="info">2</td>
				  <td class="info">3</td>
				</tr>
				<tr>
				  <td>Málaga</td>
				  <td>3</td>
				  <td>3</td>
				</tr>
				<tr>
				  <td class="warning">Córdoba</td>
				  <td class="warning">4</td>
				  <td class="warning">3</td>
				</tr>
				<tr>
				  <td class="danger">Barcelona</td>
				  <td class="danger">5</td>
				  <td class="danger">2</td>
				</tr>
			</table>
		</div> <!-- Fin de container -->
<!-- ************************************************************************************************************* -->
<!-- ************************************************************************************************************* -->
<!-- ************************************************************************************************************** -->
		<footer id="footer" class="col-md-12">
            Football Manager FC
		</footer>
<!-- ************************************************************************************************************** -->
	</body>
</html>