<%-- 
    Document   : clasificaciones
    Created on : 30-oct-2014, 20:00:45
    Author     : Juan Enrique
--%>

<%@page import="java.util.TreeMap"%>
<%@page import="java.util.SortedMap"%>
<%@page import="entities.Temporada"%>
<%@page import="entities.HistorialLiga"%>
<%@page import="entities.Clasificacion"%>
<%@page import="entities.Partido"%>
<%@page import="entities.Equipo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%!
    private Map<String, List<SortedMap<Integer, Equipo>>> clasificaciones;
    private Map<String, List<List<Partido>>> resultados;
    private List<Equipo> equipos;
%>

<%
    /* RECEPCIÓN DE OBJETOS DEL SERVLET */
    equipos = (List<Equipo>) request.getAttribute("equipos");
    Temporada tempActual = (Temporada) request.getAttribute("tempActual");
    List<HistorialLiga> ligasequipos = (List<HistorialLiga>) request.getAttribute("ligasequipos");
    List<Partido> partidos = (List<Partido>) request.getAttribute("partidos");
    List<Clasificacion> posiciones = (List<Clasificacion>) request.getAttribute("posiciones");
    List<Equipo> equipos = (List<Equipo>) request.getAttribute("equipos");

    /* COLECCIONES PARA ALMACENAR CLASIFICACIONES */
    clasificaciones = new HashMap<String, List<SortedMap<Integer, Equipo>>>();
    clasificaciones.put("Prebenjamín", new ArrayList<SortedMap<Integer, Equipo>>());
    clasificaciones.put("Benjamín", new ArrayList<SortedMap<Integer, Equipo>>());
    clasificaciones.put("Alevín", new ArrayList<SortedMap<Integer, Equipo>>());
    clasificaciones.put("Infantil", new ArrayList<SortedMap<Integer, Equipo>>());
    clasificaciones.put("Cadete", new ArrayList<SortedMap<Integer, Equipo>>());
    clasificaciones.put("Juvenil", new ArrayList<SortedMap<Integer, Equipo>>());
    clasificaciones.put("Senior", new ArrayList<SortedMap<Integer, Equipo>>());

    for (List l : clasificaciones.values()) {
        for (int i = 0; i < 4; i++) {
            l.add(new TreeMap<Integer, Equipo>());
        }
    }

    Equipo eq = null;
    for (Clasificacion p : posiciones) {
        eq = findEquipo(p.getEquipo());
        if (eq != null) {
            for (HistorialLiga h : ligasequipos) {
                if (h.getEquipo().equals(eq) && h.getTemporada().equals(tempActual)) {
                    clasificaciones.get(eq.getCategoria()).get(h.getLiga().getId() - 1).put(p.getPosicion(), eq);
                }
            }
        }
    }

    /* COLECCIONES PARA ALMACENAR RESULTADOS */
    resultados = new HashMap<String, List<List<Partido>>>();
    resultados.put("Prebenjamín", new ArrayList<List<Partido>>());
    resultados.put("Benjamín", new ArrayList<List<Partido>>());
    resultados.put("Alevín", new ArrayList<List<Partido>>());
    resultados.put("Infantil", new ArrayList<List<Partido>>());
    resultados.put("Cadete", new ArrayList<List<Partido>>());
    resultados.put("Juvenil", new ArrayList<List<Partido>>());
    resultados.put("Senior", new ArrayList<List<Partido>>());

    for (List li : resultados.values()) {
        for (int i = 0; i < 4; i++) {
            li.add(new ArrayList<Partido>());
        }
    }

    for (Partido p : partidos) {
        if (p.getTemporadaIdtemporada().equals(tempActual)) {
            for (HistorialLiga h : ligasequipos) {
                if (h.getEquipo().equals(p.getEquipo()) && h.getTemporada().equals(tempActual)) {
                    resultados.get(p.getEquipo().getCategoria()).get(h.getLiga().getId() - 1).add(p);
                }
            }
        }
    }
%>

<%!
    //Función para obtener el equipo a partir del nombre
    public Equipo findEquipo(String name) {
        for (Equipo e : equipos) {
            if (e.getNombre().equals(name)) {
                return e;
            }
        }
        return null;
    }
%>

<%!
    public String generarClasificacion(String categoria, int division) {
        String res = "";
        SortedMap<Integer, Equipo> clasificacion = clasificaciones.get(categoria).get(division - 1);
        if (!clasificacion.isEmpty()) {
            res += "<div class=\"col-md-6\"><h3>Clasificación</h3><div class=\"clasificacion\"><table><tr><td>Posición</td><td >Equipo</td></tr>";
            for (Integer i : clasificacion.keySet()) {
                res += "<tr><td>" + (i) + "</td><td>" + clasificacion.get(i).getNombre() + "</td></tr>";
            }
            res += "</table></div></div>";
        }
        return res;
    }

%>

<%!    public String generarResultados(String categoria, int division) {
        String res = "";
        List<Partido> encuentros = resultados.get(categoria).get(division - 1);
        if (!encuentros.isEmpty()) {
            res += "<div class=\"col-md-6\"><h3>Resultados</h3><div class=\"resultados\"><table><tr><td>Local</td><td>Resultado</td><td>Visitante</td></tr>";
            for (Partido p : encuentros) {
                res += "<tr><td>" + p.getEquipo().getNombre() + "</td><td>" + p.getResultado() + "</td><td>" + p.getEquipoRival() + "</td></tr>";
            }
            res += "</table></div></div>";
        }
        return res;
    }

%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="css/bootstrap.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="css/bootstrap-theme.css">

        <link rel="stylesheet" href="css/propio.css">

        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">

        <script src="js/jquery-2.1.1.min.js"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="js/bootstrap.js"></script>
    </head>

    <body background="images/background2.jpg">
        <!-- ************************************************************************************************************** -->
        <nav class="navbar navbar-inverse" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><i class="fa fa-futbol-o"></i> Manager FC</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.request.contextPath}/entrenamientos"><i class="fa fa-soccer-ball-o"></i> Entrenamientos</a></li>
                        <li><a href="${pageContext.request.contextPath}/jornadas"><i class="fa fa-calendar"></i> Jornadas</a></li>
                        <li class="active"><a href="${pageContext.request.contextPath}/clasificaciones"><i class="fa fa-line-chart"></i> Clasificaciones</a></li>
                        <li><a href="${pageContext.request.contextPath}/historico"><i class="fa fa-history"></i> Histórico</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="${pageContext.request.contextPath}"><img id="logo" src="images/logo.png"></img></a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <!-- ************************************************************************************************************* -->
        <!-- *************************************************CONTENIDO*************************************************** -->
        <!-- ************************************************************************************************************* -->
        <div id="principal" class="container">
            <div class="page-header">
                <h1><i class="fa fa-line-chart"></i> Clasificación y resultados</h1>
            </div>
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#prebenjamin" role="tab" data-toggle="tab">Prebenjamín</a></li>
                <li><a href="#benjamin" role="tab" data-toggle="tab">Benjamín</a></li>
                <li><a href="#alevin" role="tab" data-toggle="tab">Alevín</a></li>
                <li><a href="#infantil" role="tab" data-toggle="tab">Infantil</a></li>
                <li><a href="#cadete" role="tab" data-toggle="tab">Cadete</a></li>
                <li><a href="#juvenil" role="tab" data-toggle="tab">Juvenil</a></li>
                <li><a href="#senior" role="tab" data-toggle="tab">Senior</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="prebenjamin">
                    <div class="panel-group division" id="divsprebenjamin">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divsprebenjamin" href="#divunoprebenjamin">
                                        Primera división
                                    </a>
                                </h4>
                            </div>
                            <div id="divunoprebenjamin" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Prebenjamín", 1)); %>
                                    <% out.println(generarResultados("Prebenjamín", 1)); %>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divsprebenjamin" href="#divdosprebenjamin">
                                        Segunda división
                                    </a>
                                </h4>
                            </div>
                            <div id="divdosprebenjamin" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Prebenjamín", 2)); %>
                                    <% out.println(generarResultados("Prebenjamín", 2)); %>  
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divsprebenjamin" href="#divtresprebenjamin">
                                        Tercera división
                                    </a>
                                </h4>
                            </div>
                            <div id="divtresprebenjamin" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Prebenjamín", 3)); %>
                                    <% out.println(generarResultados("Prebenjamín", 3)); %>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divsprebenjamin" href="#divcuatroprebenjamin">
                                        Cuarta división
                                    </a>
                                </h4>
                            </div>
                            <div id="divcuatroprebenjamin" class="panel-collapse collapse">
                                <div class="panel-body">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- BENJAMIN -->

                <div class="tab-pane fade" id="benjamin">
                    <div class="panel-group division" id="divsbenjamin">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divsbenjamin" href="#divunobenjamin">
                                        Primera división
                                    </a>
                                </h4>
                            </div>
                            <div id="divunobenjamin" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Benjamín", 1)); %>
                                    <% out.println(generarResultados("Benjamín", 1)); %>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divsbenjamin" href="#divdosbenjamin">
                                        Segunda división
                                    </a>
                                </h4>
                            </div>
                            <div id="divdosbenjamin" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Benjamín", 2)); %>
                                    <% out.println(generarResultados("Benjamín", 2)); %>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divsbenjamin" href="#divtresbenjamin">
                                        Tercera división
                                    </a>
                                </h4>
                            </div>
                            <div id="divtresbenjamin" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Benjamín", 3)); %>
                                    <% out.println(generarResultados("Benjamín", 3)); %>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divsbenjamin" href="#divcuatrobenjamin">
                                        Cuarta división
                                    </a>
                                </h4>
                            </div>
                            <div id="divcuatrobenjamin" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Benjamín", 4)); %>
                                    <% out.println(generarResultados("Benjamín", 4)); %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ALEVIN -->

                <div class="tab-pane fade" id="alevin">
                    <div class="panel-group division" id="divsalevin">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divsalevin" href="#divunoalevin">
                                        Primera división
                                    </a>
                                </h4>
                            </div>
                            <div id="divunoalevin" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Alevín", 1)); %>
                                    <% out.println(generarResultados("Alevín", 1)); %>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divsalevin" href="#divdosalevin">
                                        Segunda división
                                    </a>
                                </h4>
                            </div>
                            <div id="divdosalevin" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Alevín", 2)); %>
                                    <% out.println(generarResultados("Alevín", 2)); %>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divsalevin" href="#divtresalevin">
                                        Tercera división
                                    </a>
                                </h4>
                            </div>
                            <div id="divtresalevin" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Alevín", 3)); %>
                                    <% out.println(generarResultados("Alevín", 3)); %>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divsalevin" href="#divcuatroalevin">
                                        Cuarta división
                                    </a>
                                </h4>
                            </div>
                            <div id="divcuatroalevin" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Alevín", 4)); %>
                                    <% out.println(generarResultados("Alevín", 4)); %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- INFANTIL -->


                <div class="tab-pane fade" id="infantil">
                    <div class="panel-group division" id="divsinfantil">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divsinfantil" href="#divunoinfantil">
                                        Primera división
                                    </a>
                                </h4>
                            </div>
                            <div id="divunoinfantil" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Infantil", 1)); %>
                                    <% out.println(generarResultados("Infantil", 1)); %>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divsinfantil" href="#divdosinfantil">
                                        Segunda división
                                    </a>
                                </h4>
                            </div>
                            <div id="divdosinfantil" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Infantil", 2)); %>
                                    <% out.println(generarResultados("Infantil", 2)); %>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divsinfantil" href="#divtresinfantil">
                                        Tercera división
                                    </a>
                                </h4>
                            </div>
                            <div id="divtresinfantil" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Infantil", 3)); %>
                                    <% out.println(generarResultados("Infantil", 3)); %>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divsinfantil" href="#divcuatroinfantil">
                                        Cuarta división
                                    </a>
                                </h4>
                            </div>
                            <div id="divcuatroinfantil" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Infantil", 4)); %>
                                    <% out.println(generarResultados("Infantil", 4)); %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- CADETE -->

                <div class="tab-pane fade" id="cadete">
                    <div class="panel-group division" id="divscadete">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divscadete" href="#divunocadete">
                                        Primera división
                                    </a>
                                </h4>
                            </div>
                            <div id="divunocadete" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Cadete", 1)); %>
                                    <% out.println(generarResultados("Cadete", 1)); %>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divscadete" href="#divdoscadete">
                                        Segunda división
                                    </a>
                                </h4>
                            </div>
                            <div id="divdoscadete" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Cadete", 2)); %>
                                    <% out.println(generarResultados("Cadete", 2)); %>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divscadete" href="#divtrescadete">
                                        Tercera división
                                    </a>
                                </h4>
                            </div>
                            <div id="divtrescadete" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Cadete", 3)); %>
                                    <% out.println(generarResultados("Cadete", 3)); %>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divscadete" href="#divcuatrocadete">
                                        Cuarta división
                                    </a>
                                </h4>
                            </div>
                            <div id="divcuatrocadete" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Cadete", 4)); %>
                                    <% out.println(generarResultados("Cadete", 4)); %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- JUVENIL -->

                <div class="tab-pane fade" id="juvenil">
                    <div class="panel-group division" id="divsjuvenil">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divsjuvenil" href="#divunojuvenil">
                                        Primera división
                                    </a>
                                </h4>
                            </div>
                            <div id="divunojuvenil" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Juvenil", 1)); %>
                                    <% out.println(generarResultados("Juvenil", 1)); %>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divsjuvenil" href="#divdosjuvenil">
                                        Segunda división
                                    </a>
                                </h4>
                            </div>
                            <div id="divdosjuvenil" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Juvenil", 2)); %>
                                    <% out.println(generarResultados("Juvenil", 2)); %>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divsjuvenil" href="#divtresjuvenil">
                                        Tercera división
                                    </a>
                                </h4>
                            </div>
                            <div id="divtresjuvenil" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Juvenil", 3)); %>
                                    <% out.println(generarResultados("Juvenil", 3)); %>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divsjuvenil" href="#divcuatrojuvenil">
                                        Cuarta división
                                    </a>
                                </h4>
                            </div>
                            <div id="divcuatrojuvenil" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Juvenil", 4)); %>
                                    <% out.println(generarResultados("Juvenil", 4)); %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- SENIOR -->

                <div class="tab-pane fade" id="senior">
                    <div class="panel-group division" id="divssenior">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divssenior" href="#divunosenior">
                                        Primera división
                                    </a>
                                </h4>
                            </div>
                            <div id="divunosenior" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Senior", 1)); %>
                                    <% out.println(generarResultados("Senior", 1)); %>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divssenior" href="#divdossenior">
                                        Segunda división
                                    </a>
                                </h4>
                            </div>
                            <div id="divdossenior" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Senior", 2)); %>
                                    <% out.println(generarResultados("Senior", 2)); %>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divssenior" href="#divtressenior">
                                        Tercera división
                                    </a>
                                </h4>
                            </div>
                            <div id="divtressenior" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Senior", 3)); %>
                                    <% out.println(generarResultados("Senior", 3)); %>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#divssenior" href="#divcuatrosenior">
                                        Cuarta división
                                    </a>
                                </h4>
                            </div>
                            <div id="divcuatrosenior" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <% out.println(generarClasificacion("Senior", 4)); %>
                                    <% out.println(generarResultados("Senior", 4));%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ************************************************************************************************************* -->
        <!-- ************************************************************************************************************* -->
        <!-- ************************************************************************************************************** -->
        <footer id="footer" class="col-md-12">
            Football Manager FC
        </footer>
        <!-- ************************************************************************************************************** -->
    </body>
</html>
