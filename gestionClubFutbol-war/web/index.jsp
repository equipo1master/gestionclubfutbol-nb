<html>
    <head>
        <meta charset="utf-8">
        <title>Manager FC</title>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="css/bootstrap.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="css/bootstrap-theme.css">

        <link rel="stylesheet" href="css/propio.css">

        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">

        <script src="js/jquery-2.1.1.min.js"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="js/bootstrap.js"></script>
    </head>

    <body background="images/background2.jpg">
        <!-- ************************************************************************************************************** -->
        <nav class="navbar navbar-inverse" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="${pageContext.request.contextPath}"><i class="fa fa-futbol-o"></i> Manager FC</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.request.contextPath}/entrenamientos"><i class="fa fa-soccer-ball-o"></i> Entrenamientos</a></li>
                        <li><a href="${pageContext.request.contextPath}/jornadas"><i class="fa fa-calendar"></i> Jornadas</a></li>
                        <li><a href="${pageContext.request.contextPath}/clasificaciones"><i class="fa fa-line-chart"></i> Clasificaciones</a></li>
                        <li><a href="${pageContext.request.contextPath}/historico"><i class="fa fa-history"></i> Histórico</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="${pageContext.request.contextPath}/index.jsp"><img id="logo" src="images/logo.png"></img></a></li>
		    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <!-- ************************************************************************************************************* -->
        <!-- *************************************************CONTENIDO*************************************************** -->
        <!-- ************************************************************************************************************* -->
            <div class="jumbotron">
                <h1><i class="fa fa-futbol-o"></i> FC Manager - Histórico</h1>
                <p>Revisa los históricos de todos los partidos de todas las temporadas de tu equipo. Revisa sus alieneaciones, los goleadores y los resultados de tu Equipo!</p>
            </div>
        <!-- ************************************************************************************************************* -->
        <!-- ************************************************************************************************************* -->
        <!-- ************************************************************************************************************** -->

        <!-- ************************************************************************************************************** -->
    </body>
</html>