
<%@page import="java.util.Collection"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.HashMap"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="entities.Entrenamiento"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<%@page import="java.util.List"%>
<%@page import="entities.Equipo"%>

<html>
    <head>
        <meta charset="utf-8">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="css/bootstrap.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="css/bootstrap-theme.css">

        <link rel="stylesheet" href="css/propio.css">

        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">

        <script src="js/jquery-2.1.1.min.js"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="js/bootstrap.js"></script>

        <script type="text/javascript" src="js/nav.js"></script>
    </head>

    <body background="images/background2.jpg">
        <!-- ************************************************************************************************************** -->
        <nav class="navbar navbar-inverse" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><i class="fa fa-futbol-o"></i> Manager FC</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="${pageContext.request.contextPath}/entrenamientos"><i class="fa fa-soccer-ball-o"></i> Entrenamientos</a></li>
                        <li><a href="${pageContext.request.contextPath}/jornadas"><i class="fa fa-calendar"></i> Jornadas</a></li>
                        <li><a href="${pageContext.request.contextPath}/clasificaciones"><i class="fa fa-line-chart"></i> Clasificaciones</a></li>
                        <li><a href="${pageContext.request.contextPath}/historico"><i class="fa fa-history"></i> Hist�rico</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="${pageContext.request.contextPath}"><img id="logo" src="images/logo.png"></img></a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <!-- ************************************************************************************************************* -->
        <!-- *************************************************CONTENIDO*************************************************** -->
        <!-- ************************************************************************************************************* -->
        <div id="principal" class="container">
            <div class="page-header">
                <h1><i class="fa fa-clock-o"></i> Horarios de entrenamiento</h1>
            </div>
            <span class="ir-arriba icon-arrow-up2">Ir arriba</span>
            <div style="margin: 5%;">
                Selecciona el equipo a visualizar:
                <div class="dropdown teamSelection" style="margin-left: 10px;">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                        Seleccionar equipo
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

                        <%
                            List<Equipo> equipos = (List<Equipo>) request.getAttribute("equipos");
                            for (Equipo e : equipos) {
                        %>
                        <li role="presentation"><a role="menuitem" tabindex="-1"  href="#<%out.print(e.getId());%>"><%out.println(e.getNombre());%></a></li>
                            <%  }
                            %>
                    </ul>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-12">
                        <%
                            for (Equipo e : equipos) {
                                Collection<Entrenamiento> entrenamientos = e.getEntrenamientoCollection();
                                if (entrenamientos != null && entrenamientos.size() > 0) {
                        %>
                        <div class="col-md-4" id="<%out.print(e.getId());%>">
                            <div class="teamName"><i class="fa fa-calendar"></i><span style="margin-left: 5%;"><%out.println(e.getNombre());%></span></div>
                            <table class="table table-bordered table-verde" style="border-radius: 5px;">
                                <tr>
                                    <th>D�a</th>
                                    <th>Hora</th>
                                </tr>
                                <%
                                    for (Entrenamiento entr : entrenamientos) {
                                %>
                                <tr>
                                    <td><%out.println(entr.getDiaSemana());%></td>
                                    <td><%out.println(entr.getHoraInicio() + " - " + entr.getHoraFin());%></td>
                                </tr>
                                <%  }
                                %>

                            </table>
                        </div>
                        <%
                                }
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
        <!-- ************************************************************************************************************* -->
        <!-- ************************************************************************************************************* -->
        <!-- ************************************************************************************************************** -->
        <footer id="footer" class="col-md-12">
            Football Manager FC
        </footer>
        <!-- ************************************************************************************************************** -->
    </body>
</html>
