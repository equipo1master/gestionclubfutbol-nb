<%@page import="entities.Convocatoria"%>
<%@page import="entities.Gol"%>
<%@page import="java.sql.Date"%>
<%@page import="java.util.Map"%>
<%@page import="entities.Partido"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="entities.Jugador"%>
<%@page import="java.util.List"%>
<%@page import="entities.Equipo"%>
<%@page import="entities.Temporada"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%!
<<<<<<< Updated upstream
    private Equipo equipo;
=======
    private Equipo e;
>>>>>>> Stashed changes
    private List<Equipo> equipos;
%>

<%
<<<<<<< Updated upstream
    equipo = (Equipo) request.getAttribute("equipo");
=======
    e = (Equipo) request.getAttribute("equipo");
>>>>>>> Stashed changes
    equipos = (List<Equipo>) request.getAttribute("equipos");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/propio.css">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <!-- Optional theme -->
        <link rel="stylesheet" href="css/bootstrap-theme.css">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">

        <script src="js/jquery-2.1.1.min.js"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="js/bootstrap.js"></script>

        <script type="text/javascript" src="js/nav.js"></script>
    </head>

    <body>
        <!-- ************************************************************************************************************** -->
        <span class="ir-arriba icon-arrow-up2">Ir arriba</span>
        <nav class="navbar navbar-inverse" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><i class="fa fa-shield"></i> Manager FC</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.request.contextPath}/entrenamientos"><i class="fa fa-soccer-ball-o"></i> Entrenamientos</a></li>
                        <li><a href="${pageContext.request.contextPath}/jornadas"><i class="fa fa-calendar"></i> Jornadas</a></li>
                        <li><a href="${pageContext.request.contextPath}/clasificaciones"><i class="fa fa-line-chart"></i> Clasificaciones</a></li>
                        <li class="active"><a href="${pageContext.request.contextPath}/historico"><i class="fa  fa-history"></i> Histórico</a></li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="${pageContext.request.contextPath}"><img id="logo" src="images/logo.png"></img></a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>


        <!-- ****************************************************************************************** -->
        <!-- ******************************************CONTENIDO*************************************** -->
        <!-- ****************************************************************************************** -->
        <div id="principal" class="container">
<<<<<<< Updated upstream
            <%
                /* COMPROBAMOS LOS ERRORES, SI LOS HUBIERE */
                boolean error = false;
                if (session.getAttribute("error") != null && !((List<String>) session.getAttribute("error")).isEmpty()) {
                    error = true;
                    for (String errorMensaje : (List<String>) session.getAttribute("error")) {
                        out.println("<div class=\"alert alert-danger alert-dismissable margin-top-s\""
                                + " role=\"alert\"><button type=\"button\""
                                + " class=\"close\" data-dismiss=\"alert\">&times;</button>" + errorMensaje + "</div>");
                    }
                }
                session.removeAttribute("error");

            %>
            <div class="page-header">
                <h1><i class="fa fa-history"></i> Histórico <small>Equipos</small></h1>
            </div>
            <form method="GET" action="historico">
                <!-- ****************************************************************************************** -->
                <!-- ******************************************EQUIPOS***************************************** -->
                <!-- ****************************************************************************************** -->
                <div style="min-height: 50px">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default">Prebenjamín</button>
                        </div>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default">Benjamín</button>
                        </div>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default">Alevín</button>
                        </div>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default">Infantil</button>
                        </div>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default">Cadete</button>
                        </div>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default">Senior</button>
                        </div>
                    </div>
                    <div style="float: left" class="input-group col-md-10">
                        <span class="input-group-addon"><i class="fa fa-shield"></i></span>
                        <input type="text" class="form-control" placeholder="Equipo">
                    </div>
                    <div style="float: right; padding: 0px;" class="col-md-2">
                        <button type="submit" class="btn btn-relieve-cesped col-md-12">Enviar</button>
                    </div>
                </div>
            </form>

            <%                            if (!error) {

                }
                if (!error) {
            %>

            <!-- ****************************************************************************************** -->
            <!-- ******************************************SELECTOR**************************************** -->
            <!-- ****************************************************************************************** -->
            <div class="dropdown">
                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                    Seleccionar temporada
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#2014">Temporada 2014/2015</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#2013">Temporada 2013/2014</a></li>
                </ul>
            </div>
            
            <div id="2014" >
                <h1><small><% 
                out.println(equipo.getNombre());
                %></small></h1>
                <h2>Temporada 2013-2014</h2>
                
                <!-- ****************************************************************************************** -->
                        <!-- ************************************INFORMACIÓN GENERAL*********************************** -->
                        <!-- ****************************************************************************************** -->
                        <div class="page-header margen-superior">
                            <h3><i class="fa fa-info"></i> Información del Equipo</h3>
                        </div>

=======
            
            <div class="page-header">
                <h1><i class="fa fa-history"></i> Histórico <small>Equipos</small></h1>
            </div>


            
            <% 
                for (Equipo equipo : equipos){
            %>
            <div id="2014">
                <h1><small><% 
                out.println(equipo.getNombre());
                %></small></h1>
                <h2>Temporada 2013-2014</h2>
                
                <!-- ****************************************************************************************** -->
                        <!-- ************************************INFORMACIÓN GENERAL*********************************** -->
                        <!-- ****************************************************************************************** -->
                        <div class="page-header margen-superior">
                            <h3><i class="fa fa-info"></i> Información del Equipo</h3>
                        </div>

>>>>>>> Stashed changes
                        <div id="atributos-historico" class="col-md-12">
                            <div class="col-md-6">
                                <ul>
                                    <ol><i class="fa fa-shield"></i> Equipo: <%=equipo.getNombre() %></ol>
                                    <ol><i class="fa fa-child"></i> Entrenador: <%=equipo.getEntrenador().getNombreCompleto() %></ol>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul>
                                    <ol><i class="fa fa-graduation-cap"></i> Categoría: <%=equipo.getCategoria() %></ol>
                                    <ol><i class="fa fa-line-chart"></i> Clasificación: 1</ol>
                                    <ol><i class="fa fa-user-md"></i> Delegado: </ol>
                                </ul>
                            </div>
                        </div>

                        <!-- ****************************************************************************************** -->
                        <!-- ****************************************JUGADORES***************************************** -->
                        <!-- ****************************************************************************************** -->
                        <div class="page-header">
                            <h3><i class="fa fa-group"></i> Jugadores</h3>
<<<<<<< Updated upstream
                        </div>
                        <div class="col-md-12">
                            <table class="table table-bordered table-verde">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Fecha nacimiento</th>
                                        <th>Categoría </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                    for (Jugador j :equipo.getJugadorCollection()){
                                    %>
                                    <tr>
                                        <td><%=j.getNombreCompleto() %></td>
                                        <td><%=new SimpleDateFormat("dd-MM-yyyy").format(j.getFechaNacimiento()) %></td>
                                        <td><%=equipo.getCategoria() %></td>
                                    </tr>
                                    <%
                                    }    
                                    %>
                                </tbody>
                            </table>
                        </div>
=======
                        </div>
                        <div class="col-md-12">
                            <table class="table table-bordered table-verde">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Fecha nacimiento</th>
                                        <th>Categoría </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                    for (Jugador j :equipo.getJugadorCollection()){
                                    %>
                                    <tr>
                                        <td><%=j.getNombreCompleto() %></td>
                                        <td><%=new SimpleDateFormat("dd-MM-yyyy").format(j.getFechaNacimiento()) %></td>
                                        <td><%=equipo.getCategoria() %></td>
                                    </tr>
                                    <%
                                    }    
                                    %>
                                </tbody>
                            </table>
                        </div>
>>>>>>> Stashed changes
                
            </div>
            <div class="panel-group division" id="temp2014">
                <%  // FOR EACH JORNADA
                for (Partido p : equipo.getPartidoCollection()) {
                    //if(p.getTemporadaIdtemporada().getIdtemporada().toString().equals("2012"))
                    {
                %>    
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#temp2014" href="#jornada1">
                            </a>
                        </h4>
                    </div>
                    <div id="jornada1" class="selector-jornada panel-collapse collapse in">
                        
                        <!-- ****************************************************************************************** -->
                        <!-- *****************************************GOLEADORES*************************************** -->
                        <!-- ****************************************************************************************** -->
                        
                        <div class="resultado-partido texto-centrado">
                            <% if(p.getJuegoEnCasa()){
                                out.println(p.getEquipo().getNombre()+" "+ p.getResultado()+ " " + p.getEquipoRival());
                            }else{
                                out.println(p.getEquipoRival()+" "+ p.getResultado()+ " " +p.getEquipo().getNombre());
                            }
                            %>
                        </div>

                        <div class="page-header">
                            <h3><i class="fa fa-soccer-ball-o"></i> Goleadores</h3>
                        </div>
                        <div class="col-md-12">
                            <table class="table table-bordered table-verde">
                                <thead>
                                    <tr>
                                        <th>Equipo</th>
                                        <th>Jugador</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% 
                                    for (Gol g : p.getGolCollection()){
                                        
                                    %>
                                    <tr>
                                        <td><%=g.getPuntuacion() %></td>
                                        <td><%=g.getJugadorId().getNombreCompleto() %></td>
                                    </tr>
                                    <% 
                                    }
                                    %>
                                </tbody>
                            </table>
                        </div>
                        <!-- ****************************************************************************************** -->
                        <!-- ****************************************CONVOCATORIA************************************** -->
                        <!-- ****************************************************************************************** -->
                        <div class="panel-body">
                            <div class="page-header">
                                <h3><i class="fa fa-group"></i> Convocatoria</h3>
                            </div>
                            <table class="table table-bordered table-pizarra table-verde">
                                <thead>
                                    <tr>
                                        <th><i></i></th>
                                        <th>Nombre</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                    int i;
                                    for (Convocatoria c : p.getConvocatoriaCollection()){
                                        i = 1;
                                        for (Jugador j : c.getJugadorCollection()){
                                    %>
                                    <tr>
                                        <td><%= i++ %></td>
                                        <td><%= j.getNombreCompleto() %></td>
                                    </tr>
                                    <%
                                        } 
                                    }
                                    %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <% // END JORNADAS
                    }  
                }
                %>
            </div>
<<<<<<< Updated upstream
            <div id="2013" class="page-header">
                <h1><small>Temporada 2013/2014</small></h1>
            </div>
            <div class="panel-group division" id="temp2013">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#temp2013" href="#jornada5">
                                Jornada 5 (Atlético de Madrid vs Levante), temporada 2013/2014
                            </a>
                        </h4>
                    </div>
                    <div id="jornada5" class="panel-collapse collapse">
                        <div class="panel-body">
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#temp2013" href="#jornada5">
                                Jornada 5 (Atlético de Madrid vs Levante), temporada 2013/2014
                            </a>
                        </h4>
                    </div>
                    <div id="jornada5" class="panel-collapse collapse">
                        <div class="panel-body">
                        </div>
                    </div>
                </div>
            </div>
            <%
            } else {
            %>
            <div class="jumbotron">
                <h1><i class="fa fa-futbol-o"></i> FC Manager - Histórico</h1>
                <p>Revisa los históricos de todos los partidos de todas las temporadas de tu equipo. Revisa sus alieneaciones, los goleadores y los resultados de tu Equipo!</p>
            </div>
            <%
=======

            <%  // END EQUIPOS
>>>>>>> Stashed changes
                }
            %>
        </div> <!-- Fin de container -->
        <!-- ************************************************************************************************************* -->
        <!-- ************************************************************************************************************* -->
        <!-- ************************************************************************************************************** -->
        <footer id="footer" class="col-md-12">
<<<<<<< Updated upstream
            Football Manager 2014
=======
            Football Manager FC
>>>>>>> Stashed changes
        </footer>
        <!-- ************************************************************************************************************** -->
    </body>
</html>
